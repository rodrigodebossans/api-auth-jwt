const app = require('express')();
const bodyParser = require('body-parser');

const mongoose = require('../database');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

require('./controller/')(app);

app.get('/', async (req, res) => {
  return res.status(200).send({ message: 'Hello World' });
})

app.listen(32755, '0.0.0.0', () => {
  console.log('Server is running in a port 32755...');
})
