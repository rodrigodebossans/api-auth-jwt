const express = require('express');
const bcrypt = require('bcryptjs');
const User = require('../../model/user/user.model');
const mailer = require('../../modules/mailer');
const authConfig = require('../../config/auth/auth.config');

const router = express.Router();

router.post('/register', async (req, res) => {
  try {
    const { name, email, password } = req.body;

    // retorne erro caso o objeto não corresponda com o modelo
    if (!(name && email && password))
      throw new String('Expected object not informed');

    // retore erro caso o usuário já exista
    if (await User.findOne({ email })) throw new String('User already exists');

    const createdUser = await User.create({ name, email, password });

    // retorne erro caso a haja falha na criação do usuário
    if (!createdUser) throw new String('Failed to create user');

    // elimine a senha da resposta da requisição
    createdUser.password = undefined;

    // gere um token com 60 seg de validade
    const token = authConfig.generateToken(createdUser.id, (err) => {
      // retorne erro caso haja falhe a geração do token
      if (err) throw new String('Error on generate token, try again');
    });

    return res.status(200).send({ createdUser, token });
  } catch (err) {
    if (err instanceof String) return res.status(400).send({ error: err });

    return res.status(400).send({ error: 'Registration failed' });
  }
});

router.post('/authenticate', async (req, res) => {
  try {
    const { email, password } = req.body;

    // retorne erro caso o objeto não corresponda com o modelo
    if (!(email && password)) throw new String('Expedted object not informed');

    // busque o usuário na base pelo e-mail informado
    const foundUser = await User.findOne({ email }).select('+password');

    // retorne erro caso o usuário não seja encontrado
    if (!foundUser) throw new String('User not found');

    // retorne erro caso a senha informada não seja igual a senha cadastrada
    if (!(await bcrypt.compare(password, foundUser.password)))
      throw new String('Invalid password');

    // gere um token com 60 seg de validade e retorne erro caso haja falhe a geração do token
    const token = authConfig.generateToken(foundUser.id, (err) => {
      if (err) throw new String('Error on generate token, try again');
    });

    const editedUser = await User.findByIdAndUpdate(foundUser.id, {
      $set: { lastAccess: Date.now() }
    });

    return res.send({ user: editedUser, token });
  } catch (err) {
    console.log('catch =>', err);
    if (err instanceof String) return res.status(401).send({ error: err });

    return res.status(401).send({ error: 'Authenticate failed' });
  }
});

router.post('/forgot-password', async (req, res) => {
  try {
    const { email } = req.body;

    // retorne erro caso o objeto não corresponda com o modelo esperado
    if (!email) throw new String('Expedted object not informed');

    // busque o usuário na base pelo e-mail informado
    const foundUser = await User.findOne({ email });

    // retorne erro caso o usuário não seja encontrado
    if (!foundUser) throw new String('User not found');

    // gere um token para o reset password e retorne erro caso haja falhe a geração do token
    const token = authConfig.generatePasswordResetExpires(20, (err) => {
      if (err) throw new String('Unknown error');
    });

    // gere uma data de expiração para o token gerado
    const expires = new Date();
    expires.setMinutes(
      expires.getMinutes() + authConfig.PASSWORD_RESET_EXPIRES_HOURS
    );

    // insira o token e a data de inspiração no banco de dados
    const updatedUser = await User.findByIdAndUpdate(foundUser.id, {
      $set: {
        passwordResetToken: token,
        passwordResetExpires: expires,
      },
    }).select('+passwordResetToken +passwordResetExpires');

    // retorne um erro caso haja falha na atualização dos dados no banco
    if (!updatedUser) throw new String('Failed on updated user');

    await mailer.sendMail(
      {
        to: email,
        subject: 'Forgot Password',
        from: 'rodrigo@roddesign.com.br',
        template: '/forgot-password/index',
        context: {
          name: foundUser.name,
          token,
          date: new Date().toDateString(),
          hours: new Date().getHours(),
        },
      },
      (err) => {
        if (err) throw new String(`Failed on send email to ${email}`);
      }
    );

    return res.send({ emailSend: true });
  } catch (err) {
    if (err instanceof String) return res.status(400).send({ error: err });

    return res
      .status(400)
      .send({ error: 'failed on forgot password, try again' });
  }
});

router.post('/reset-password', async (req, res) => {
  try {
    const { email, password, token } = req.body;

    // retorne erro caso o objeto não corresponda com o modelo
    if (!(email && password && token))
      throw new String('Expedted object not informed');

    // busque o usuário na base pelo e-mail informado
    const foundUser = await User.findOne({ email }).select(
      '+passwordResetToken +passwordResetExpires +password'
    );

    if (!foundUser) throw new String('User not found');

    if (token !== foundUser.passwordResetToken)
      throw new String('Token incorrect');

    // retorne erro caso a senha informada não seja igual a senha cadastrada
    if (await bcrypt.compare(password, foundUser.password))
      throw new String(
        'The new password must be different from the current password'
      );

    const now = new Date();

    if (foundUser.passwordResetExpires < now)
      return res.status(400).send({
        error: 'Token expired',
        expiredAt: foundUser.passwordResetExpires.toISOString(),
      });

    foundUser.password = password;

    const modifiedUser = await foundUser.save();

    foundUser.password = undefined;

    if (!modifiedUser) throw new String('Failed on save password');

    return res.send({ modifiedUser });
  } catch (err) {
    console.log('catch err => ', err);
    if (err instanceof String) return res.status(400).send({ error: err });

    return res
      .status(400)
      .send({ error: 'Failed on reset password, try again' });
  }
});

module.exports = (app) => app.use('/auth', router);
