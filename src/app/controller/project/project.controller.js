const express = require('express');
const authMiddleware = require('../../middlewares/auth/auth.middleware');

const router = express.Router();

//router.use(authMiddleware);

router.post('/', async (req, res) => {
    res.status(200).send({ ok: true });
});


module.exports = app => app.use('/projects', router);