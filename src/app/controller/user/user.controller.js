const express = require('express');
const bcrypt = require('bcryptjs');
const User = require('../../model/user/user.model');
const authMiddleware = require('../../middlewares/auth/auth.middleware');
const queryparams = require('../../middlewares/util/queryparams.middleware');

const router = express.Router();

router.use(authMiddleware);
router.use(queryparams);

router.get('/', async (req, res) => {
  try {
    const users = await User.find({}, null, req.queryParams);

    return res.send({
      content: {
        users,
        lenth: users.length,
      },
    });
  } catch (err) {
    if (err) return res.status(400).send({ error: 'Failed on find users' });
  }
});

router.put('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const { name, email, password } = req.body;

    // retorne erro caso o id não seja fornecido
    if (!id) throw new String('Id no provided');

    // retorne erro caso o objeto não corresponda com o modelo
    if (!(name || email || password))
      throw new String('Expected object not informed');

    // procure pelo usuário com o id informado
    const foundUser = await User.findById(id).select('+password');

    // retorne um erro caso o usuário não seja encontrado
    if (!foundUser) throw new String('User not found');

    // se informado, altere o nome do usuário
    if (name) foundUser.name = name;

    // se informado, altere o email do usuário
    if (email) foundUser.email = email;

    // se informado, altere a senha do usuário
    if (password) {
      // retorne erro caso a senha informada não seja igual a senha cadastrada
      if (await bcrypt.compare(password, foundUser.password))
        throw new String('The new password must be different from the current password');

      foundUser.password = password;
    }

    // registre a data da modificação e salve os dados
    foundUser.modifiedAt = new Date();
    const modifiedUser = await foundUser.save();

    // retorne erro caso haja erro na alteração da senha;
    if (!modifiedUser) throw new String('Failed on save password');

    // elimine a senha da resposta da requisição
    modifiedUser.password = undefined;

    return res.status(200).send({ user: modifiedUser });
  } catch (err) {
    if (err instanceof String) return res.status(400).send({ error: err });

    if (err) return res.status(400).send({ error: 'Failed on edit user' });
  }
});

router.delete('/:id', async (req, res) => {
  try {
    const { id } = req.params;

    // retorne erro caso o id não seja fornecido
    if (!id) throw new String('Id no provided');

    // procure pelo usuário com o id informado
    const foundUser = await User.findById(id);

    // retorne um erro caso o usuário não seja encontrado
    if (!foundUser) throw new String('User not found');

    // delete o usuário
    const userDeleted = await User.findByIdAndDelete(id, (err, user) => {
      // retorne erro caso haja
      if (err) throw new String('Failed on delete user, try again');

      //retorne o usuário caso haja
      if (user) {
        user.password = undefined; // eliminando senha da resposta
        res.status(200).send({ user });
      }
    });
  } catch (err) {
    if (err instanceof String) return res.status(400).send({ error: err });

    if (err) return res.status(400).send({ error: 'Failed on delete user' });
  }
});

module.exports = (app) => app.use('/user', router);
