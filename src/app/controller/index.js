const fs = require('fs');
const path = require('path');

module.exports = app => {
    fs
        .readdirSync(__dirname)
        .filter(controllerFiles => (controllerFiles.indexOf('.') !== 0 && controllerFiles != "index.js"))
        .forEach(controllerDirs => {
            fs.readdirSync(__dirname + '/' + controllerDirs)
                .filter(controllersSubFiles => ((controllersSubFiles.indexOf('.controller.js') !== -1)))
                .forEach(controllersSubFiles => require(
                    path.resolve(__dirname, controllerDirs, controllersSubFiles)
                )(app))
        })
}
