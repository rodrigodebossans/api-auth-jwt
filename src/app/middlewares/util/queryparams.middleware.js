module.exports = (req, res, next) => {
  const page = parseInt(
    req.query.page && req.query.page > 0 ? req.query.page : 0
  );
  const limit = parseInt(
    req.query.limit && req.query.limit ? req.query.limit : 0
  );

  const skip = parseInt((page - 1) * limit > 0 ? (page - 1) * limit : 0);

  // const sort = req.query.sort ? req.query.sort.toString() : req.query.sort;

  const pageable = skip > 0 ? true : false;

  req.queryParams = {
    limit,
    skip,
    pageable
  };

  next();
};
