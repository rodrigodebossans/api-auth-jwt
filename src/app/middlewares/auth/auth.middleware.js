const jwt = require('jsonwebtoken');
const authConfig = require('../../config/auth/auth.config');
const User = require('../../model/user/user.model');

module.exports = async (req, res, next) => {
  try {
    const authHeader = req.headers.authorization;

    if (!authHeader) throw new String('No token provided');

    const parts = authHeader.split(' ');

    if (!parts.length === 2) throw new String('Token error');

    const [scheme, token] = parts;

    if (!/^Bearer$/i.test(scheme)) throw new String('Token malformatted');

    jwt.verify(token, authConfig.secret, (err, decoded) => {
      if (err) return res.send(err);

      req.id = decoded.id;
    });

    // retore erro caso o usuário não exista
    if (!(await User.findOne({ _id: req.id })))
        throw new String('User not found');

    next();
  } catch (err) {
    if (err instanceof String) return res.status(401).send({ error: err });

    return res.status(401).send({ error: 'Authentication failure' });
  }
};
