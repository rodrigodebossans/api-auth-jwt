const path = require('path');

const _TEMPLATE_PATH = path.resolve('./src/app/templates/mail/');

module.exports = {
    nodemailerOptions: {
        host: 'smtp.mailtrap.io',
        port: 2525,
        auth: {
            user: '461b2f32010519',
            pass: '1227d85e11a5db'
        }
    },
    handlebarOptions: {
        viewEngine: {
            extName: '.html',
            partialsDir: _TEMPLATE_PATH,
            layoutsDir: _TEMPLATE_PATH,
            defaultLayout: '',
        },
        viewPath: _TEMPLATE_PATH,
        extName: '.html'
    }
}
