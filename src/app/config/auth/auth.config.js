const jwt = require('jsonwebtoken');
const crypto = require('crypto');

module.exports = {
    TOKEN_SPIRATION_TIME: 3600, // time in seconds (1 hour)
    PASSWORD_RESET_EXPIRES_HOURS: 1, // time in hours (1 hour)
    secret: "cc0fbc7efba91fbe7c60ce36f139ec57", // aplication key
    generateToken: function (userId, callbackErr) { // generate token for authentication
        try {
            return jwt.sign({ id: userId }, this.secret, { expiresIn: this.TOKEN_SPIRATION_TIME });
        } catch (err) {
            return callbackErr(err);
        }
    },
    generatePasswordResetExpires: function (size, callbackErr) { // generate token for password expires
        try {
            return crypto.randomBytes(size).toString('hex');
        } catch (err) {
            return callbackErr(err);
        }
    }
}

