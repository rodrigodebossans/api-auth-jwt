interface teste {
  message?: string;
  hello(message: string): void;
}

class HelloWorld implements teste {
  constructor() {}

  hello(message: string): void {
    console.log('\n\n\tmesage =>', message, '\n\n');
  }
}

let teste: HelloWorld = new HelloWorld();

teste.hello('Eae beberes');
