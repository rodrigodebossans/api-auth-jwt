const nodemailer = require('nodemailer');
const hbs = require('nodemailer-express-handlebars');

const { nodemailerOptions, handlebarOptions } = require('../../config/mailer/mailer.config');

const transport = nodemailer.createTransport(nodemailerOptions);

transport.use('compile', hbs(handlebarOptions));

module.exports = transport;