const mongoose = require('mongoose');
require('dotenv').config();

const connOpts = {
  db: {
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
  },
  container: {
    name: process.env.SERVICE_NAME,
    port: process.env.DB_PORT,
  },
  host: process.env.SERVICE_NAME,
  port: process.env.DB_PORT,
  authSource: 'admin',
};

mongoose.connect(
  `
  mongodb://${connOpts.db.user}:${connOpts.db.password}@${connOpts.host}:${connOpts.port}/${connOpts.db.database}?authSource=${connOpts.authSource}
  `,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  }
);
mongoose.Promise = global.Promise;

module.exports = mongoose;
